Title: Example Article
Date: 2018-02-26
Category: Example Category2

# Example Markdown Page

This is an example of a Markdown page.

* Bullet point
* Another bullet point

```bash
export PIPELINE_PROJECT='<Provide the OpenShift project name for pipelines>';
export VM_PROJECT='<Specify the OpenShift project designated for VMs>';
export IPA_PRINCIPAL='<Input your IPA principal>';
export IPA_PASSWORD='<Enter your IPA password>';
export VAULT_ROLE_ID='<Provide your HashiCorp Vault role id>';
export VAULT_SECRET_ID='<Input your HashiCorp Vault secret id>';
export SSH_PUBLIC_KEY_PATH='<State the path leading to the desired public ssh key for the provisioning automation>';
export SSH_PRIVATE_KEY_PATH='<State the path leading to the desired private ssh key for the provisioning automation>';
export APP_CODE='<Specify the cmdb application code corresponding to the instance>';
export IDM_DNS_ZONE='<Enter the IDM DNS zone associated with your VM>';
```