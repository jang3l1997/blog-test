from jinja2 import Environment, FileSystemLoader, select_autoescape
import os

# Setup Jinja2 environment
env = Environment(
    loader=FileSystemLoader('templates'),
    autoescape=select_autoescape(['html', 'xml'])
)

def parse_metadata(content):
    """ Parse the metadata from the Markdown content and return it as a dictionary. """
    metadata, md_content = {}, []
    parsing_metadata = True
    for line in content.splitlines():
        if parsing_metadata:
            if line.strip() == '':
                parsing_metadata = False
            else:
                key, value = line.split(':', 1)
                metadata[key.strip()] = value.strip()
        else:
            md_content.append(line)
    return metadata, '\n'.join(md_content)

def gather_categories(directory):
    """ Gather unique categories from markdown metadata. """
    categories = set()
    for filename in os.listdir(directory):
        if filename.endswith(".md"):
            filepath = os.path.join(directory, filename)
            with open(filepath, 'r', encoding='utf-8') as file:
                content = file.read()
                metadata, _ = parse_metadata(content)
                if 'Category' in metadata:
                    categories.add(metadata['Category'])
    return categories

def render_index_page(categories):
    """ Render the index HTML page with dynamic navigation links to category pages. """
    # Ensure the public directory exists
    os.makedirs('public', exist_ok=True)
    
    rendered_content = env.get_template('index.html').render({
        'categories': list(categories)
    })
    with open(os.path.join('public', 'index.html'), 'w', encoding='utf-8') as file:
        file.write(rendered_content)
def render_category_pages(categories):
    """ Render HTML pages for each category using category_template.html. """
    for category in categories:
        # Ensure the public directory exists
        os.makedirs('public', exist_ok=True)
        
        # Retrieve articles for the category
        articles_in_category = get_articles_in_category(category)
        
        # Render category page
        rendered_content = env.get_template('category_template.html').render({
            'category': category,
            'articles': articles_in_category,
            'categories': categories
        })
        with open(os.path.join('public', f'{category}.html'), 'w', encoding='utf-8') as file:
            file.write(rendered_content)

# Directory setup
directory_path = 'content'
categories = gather_categories(directory_path)

# Render index page with category links
render_index_page(categories)

render_category_pages(categories)
